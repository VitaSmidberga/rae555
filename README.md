  Vita Šmidberga  
  141REB141  
  REMC01  
---

## [Eksāmena uzdevums 1: Izveidots video kursa POWERFUL DATA ANALYSIS WITH R pārskats iekš Wiki](https://bitbucket.org/VitaSmidberga/rae555/wiki/Datu%20anal%C4%ABze%20ar%20R)

## [Eksāmena uzdevums 2: Par pamatu datu izgūšanai kalpo: Downloading Open Data from FTP Sites, kur jūsu dati tiek ņemti no jebkura cita FTP vai HTTP datu plūsmas avota!](https://bitbucket.org/VitaSmidberga/rae555/wiki/Downloading%20Open%20Data%20from%20FTP%20Sites)
---

## Teletrafika teorijas kursa pārskats, kursa procesā apgūtais ##
---
### 1. Iepriekšējā kursa atkārtojums 
* What is queuing, queuing theory?   
    * Why is it an important tool?
    * Examples of different queuing systems
* Components of a queuing system
* The exponential distribution & queuing
* Stochastic processes  
    * Some definitions
    * The Poisson process 
* Terminology and notation
* Little’s formula
* Important queuing models with FIFO discipline
    * The M/M/1 model
    * The M/M/c model
    * The M/M/c/K model (limited queuing capacity)
    * The M/M/c/∞/N model (limited calling population)
* Priority-discipline queuing models
* Application of Queuing Theory to system design and decision making
* Simulation – What is that? 
    * Why is it an important tool?
* Building a simulation model
    * Discrete event simulation
* Structure of a BPD simulation project
* Model verification and validation
* Example – Simulation of a M/M/1 Queue

### 2. Stahostiski modeļi
* Markova ķēdes
    * TDMC modelis
    
### 3. Mobile Operator Cell modelis

### 4. Datu izplatīšanas modeļa izveidošana Kuba topoloģijas tīklā izmantojot DTMC
* PRISMA tools izmantošana
* Datu izplatīšanās straķēģijas

### 5. 10MB izplatīšana Kuba topoloģijas tīklā
* MDP modeļa veidošana ar PRISM tool palīdzību.
* Joslas Platuma parametrs (BW) un kavējuma parametrs (Delay)
* IMMRAD shēma
* Algoritmu izvēle

### 6. M/M/1:1 modeļa realizācija dažādos veidos
* Algebraiskais
* Markov Chain
* PRISM

### 7. TCPDUMP un tīkla slodzes, kas izteikta procentos, novērtēšana

### 8. TCPDUMP un tīkla slodzes novērtēšana pa trafika grupām (tcp, udp, ack, smtp)

### 9. TCPDUMP un WEKA
* Dotā trafika loga parametru novērtēšana
    * paku skaits
    * baitu skaits
    * komunikācijās iesaistīto mezglu skaits
    * komunikācijās iesaistīto portu skaits
    
### 10. TCPDUMP
* Sakarību novērtēšana starp videoplūsmu
    * skaitu
    * pārraides bitrate
    * pārraides kvalitāti

Latest tools form MIT and GIT:  
[gist.github.com](https://gist.github.com/PMaynard/144321464085fc7c3c17)  
[splunkbase.splunk.com](https://splunkbase.splunk.com/app/2748/#/details)  


